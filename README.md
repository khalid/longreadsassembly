This workflow implements a fast approach for assembling and correcting PacBio or ONT reads. 
A quick assembly is obtained with miniasm that takes all-vs-all read self-mappings results of minimap2 as input. 
The obtained contigs are then corrected using Racon. Finally medaka is used to create a consensus sequence from nanopore sequencing data. 
 
 
1. minimap2 is used for fast all-against-all overlap of raw reads
2. miniasm  "simply concatenates pieces of read sequences to generate the final sequences. Thus the per-base error rate is similar to the raw input reads."
3. raw reads are mapped back to the assembly using minimap2 
4. racon will correct the assembly given the raw reads and there mapping to miniasm assembly.
5. Medaka will call consensus given racon assembly and ONT raw reads. 

 
![alt text](./files/workflow.png "Workflow")