tabcorrection = fluidPage(

box(title = "Parameters :", width = 12, status = "primary", collapsible = TRUE, solidHeader = TRUE,

	hidden(textInput("selectcorrection", label = "", value="racon")),box(title = "Racon", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		numericInput("correction__racon_threads", label = "Number of threads to use", min = 1, max = NA, step = 1, width =  "auto", value = 4),

		p("Racon: Ultrafast consensus module for raw de novo genome assembly of long uncorrected reads "),

		p("Website : ",a(href="https://github.com/lbcb-sci/racon","https://github.com/lbcb-sci/racon",target="_blank")),

		p("Documentation : ",a(href="https://github.com/lbcb-sci/racon/blob/master/README.md","https://github.com/lbcb-sci/racon/blob/master/README.md",target="_blank")),

		p("Paper : ",a(href="https://doi.org/10.1101/gr.214270.116","https://doi.org/10.1101/gr.214270.116",target="_blank"))

	)))


