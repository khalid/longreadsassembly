tabfind_overlaps = fluidPage(

box(title = "Parameters :", width = 12, status = "primary", collapsible = TRUE, solidHeader = TRUE,

	hidden(textInput("selectfind_overlaps", label = "", value="minimap2_overlap_self")),box(title = "minimap2 overlap self", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		numericInput("find_overlaps__minimap2_overlap_self_threads", label = "Number of threads to use", min = 1, max = NA, step = 1, width =  "auto", value = 4),

		p("minimap2 overlap self: A versatile pairwise aligner for genomic and spliced nucleotide sequences"),

		p("Website : ",a(href="https://lh3.github.io/minimap2/","https://lh3.github.io/minimap2/",target="_blank")),

		p("Documentation : ",a(href="https://github.com/lh3/minimap2/blob/master/README.md","https://github.com/lh3/minimap2/blob/master/README.md",target="_blank")),

		p("Paper : ",a(href="https://doi.org/10.1093/bioinformatics/bty191","https://doi.org/10.1093/bioinformatics/bty191",target="_blank"))

	)))


