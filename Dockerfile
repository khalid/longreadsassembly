FROM mbbteam/mbb_workflows_base:latest as alltools

ENV PATH /opt/biotools/minimap2-2.17_x64-linux:$PATH 
RUN cd /opt/biotools \
 && wget https://github.com/lh3/minimap2/releases/download/v2.17/minimap2-2.17_x64-linux.tar.bz2 \
 && tar -xvjf minimap2-2.17_x64-linux.tar.bz2 \
 && rm minimap2-2.17_x64-linux.tar.bz2

ENV PATH /opt/biotools/miniasm-0.3:$PATH 
RUN cd /opt/biotools \
 && wget -O miniasm.tar.gz https://github.com/lh3/miniasm/archive/v0.3.tar.gz \
 && tar -xvzf miniasm.tar.gz \
 && cd miniasm-0.3 \
 && make -j 10 \
 && cd .. rm miniasm.tar.gz

ENV PATH /opt/biotools/racon-v1.4.10/bin:$PATH 
RUN cd /opt/biotools \
 && wget -O racon.tar.gz https://github.com/lbcb-sci/racon/releases/download/1.4.10/racon-v1.4.10.tar.gz \
 && tar -xvzf racon.tar.gz \
 && cd racon-v1.4.10 \
 && cmake -DCMAKE_BUILD_TYPE=Release \
 && make -j 10 \
 && cd .. \
 && rm racon.tar.gz

RUN wget -O htslib.tar.bz2 https://github.com/samtools/htslib/releases/download/1.9/htslib-1.9.tar.bz2 \
 && tar -xvjf htslib.tar.bz2 \
 && rm htslib.tar.bz2 \
 && cd htslib-1.9 \
 && ./configure \
 && make -j 10 \
 && make install

RUN cd /opt/biotools \
 && wget https://github.com/samtools/samtools/releases/download/1.9/samtools-1.9.tar.bz2 \
 && tar -xvjf samtools-1.9.tar.bz2 \
 && cd samtools-1.9 \
 && ./configure && make -j 10 \
 && cd .. \
 && mv samtools-1.9/samtools bin/samtools \
 && rm -r samtools-1.9 samtools-1.9.tar.bz2

RUN apt-get -y install tabix

RUN pip3 install biopython==1.76

RUN pip3 install medaka==0.10.1

RUN apt-get update

RUN apt-get install -y zlib1g-dev pkg-config libfreetype6-dev libpng-dev python-matplotlib python-setuptools

RUN cd /opt/biotools \
 && wget https://github.com/ablab/quast/releases/download/quast_5.0.2/quast-5.0.2.tar.gz \
 && tar -zxvf quast-5.0.2.tar.gz \
 && rm quast-5.0.2.tar.gz \
 && cd quast-5.0.2/ \
 && python3 ./setup.py install_full

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

#This part is necessary to run on ISEM cluster
RUN mkdir -p /share/apps/bin \
 && mkdir -p /share/apps/lib \
 && mkdir -p /share/apps/gridengine \
 && mkdir -p /share/bio \
 && mkdir -p /opt/gridengine \
 && mkdir -p /export/scrach \
 && mkdir -p /usr/lib64 \
 && ln -s /bin/bash /bin/mbb_bash \
 && ln -s /bin/bash /bin/isem_bash \
 && /usr/sbin/groupadd --system --gid 400 sge \
 && /usr/sbin/useradd --system --uid 400 --gid 400 -c GridEngine --shell /bin/true --home /opt/gridengine sge

EXPOSE 3838
CMD ["Rscript", "-e", "setwd('/sagApp/'); shiny::runApp('/sagApp/app.R',port=3838 , host='0.0.0.0')"]


FROM alltools

COPY files /workflow
COPY sagApp /sagApp

